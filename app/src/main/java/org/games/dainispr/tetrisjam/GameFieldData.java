package org.games.dainispr.tetrisjam;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.TimerTask;

public class GameFieldData {

    private int fieldwidth = 10;
    private int fieldheight = 20;
    private float cellsize=0;
    private Paint mLinePaint;
    private Paint mBackGround;
    private Paint mTextColor;
    private Paint[] mFigureColors;
    private String[] game_text = {};
    private int largest_row = -1;
    private int width;
    private int height;
    private int[][] data;
    private Rect textBounds = new Rect();
    private int droppedLines=0;
    private boolean showLevel=false;
    private int gameLevel;
    protected Bitmap [] squareBitmaps = null;
  //  private int higestRect;



    public  GameFieldData(int width, int height, Activity activity)
    {

        mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setARGB(255,230,230,230);

        mBackGround = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBackGround.setARGB(255,128,128,128);

        mTextColor = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextColor.setARGB(255,255,255,255);
       // mTextColor.setTextSize(16);
//mTextColor



        mFigureColors = new Paint[7];

        mFigureColors[0] = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFigureColors[0].setARGB(255,0,255,0);
        mFigureColors[1] = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFigureColors[1].setARGB(255,255,0,255);
        mFigureColors[2] = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFigureColors[2].setARGB(255,255,255,0);
        mFigureColors[3] = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFigureColors[3].setARGB(255,0,0,255);
        mFigureColors[4] = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFigureColors[4].setARGB(255,0,255,255);
        mFigureColors[5] = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFigureColors[5].setARGB(255,0,0,0);
        mFigureColors[6] = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFigureColors[6].setARGB(255,255,0,0);


        this.width = width;
        this.height = height;
        data = new int[height][width];

        squareBitmaps = new Bitmap[7];

        squareBitmaps[0] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.square_0);
        squareBitmaps[1] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.square_1);
        squareBitmaps[2] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.square_2);
        squareBitmaps[3] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.square_3);
        squareBitmaps[4] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.square_4);
        squareBitmaps[5] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.square_5);
        squareBitmaps[6] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.square_6);



clearData();


    }


public void setShowLevel(boolean showLevel)
{
    this.showLevel = showLevel;
}

public void setGameLevel(int gameLevel)
{

    this.gameLevel = gameLevel;
}


public  void clearData()
{
    for(int xCor=0;xCor<width;xCor++)
    {
        for(int yCor=0;yCor<height;yCor++)
            data[yCor][xCor] = 0;

    }
    droppedLines=0;

}

public int getWidth()
{
return width;

}

    public int getHeight()
    {
        return height;

    }

public int getFieldData(int xCoord,int yCoord)
{
    if(xCoord < 0 || xCoord >= fieldwidth || yCoord <0 || yCoord >= fieldheight )
        return 0;
    return data[yCoord][xCoord];
}

    public void setFieldData(int xCoord,int yCoord,int fieldData)
    {
        if(  xCoord < 0 || xCoord >= fieldwidth || yCoord <0 || yCoord >= fieldheight )
            return ;
        if(fieldData <0 || fieldData > 7)
            this.data[yCoord][xCoord] = 0;
     else {
            this.data[yCoord][xCoord] = fieldData;
        }
    }

public boolean isOnField(float xPos,float yPos,int xCoord,int yCoord)
{

    return true;
/*
    float xCellLeft = freespacewidth +  (float) xCoord*cellsize ;
   float xCellTop =  freespaceheight +  (float) yCoord*cellsize;

    return xPos > xCellLeft && xPos < xCellLeft + cellsize && yPos >  xCellTop && yPos < xCellTop + cellsize;
    */
}

public float cellRelative(float xCoord,float yCoord)
{

 return (xCoord*xCoord + yCoord*yCoord)/(cellsize*cellsize);

}

public int diffInt(float diffFloat)
{

 return  (int)(diffFloat/cellsize);
}

public float diffFloat(int diffInt)
{

 return cellsize*(float) diffInt;
}

public void setFieldText(String [] fieldText)
{
if(fieldText.length == 0)
    largest_row = -1;
else {
   int rowlen = 0;
  for(int rowind=0;rowind<fieldText.length;rowind++)
      if(rowlen<fieldText[rowind].length()) {
          rowlen = fieldText[rowind].length();
          largest_row = rowind;
      }

}

    game_text = fieldText;

}

public int getDroppedLines()
{

    return droppedLines;
}

protected void clearAbowe(int yCoord)
{
  while(yCoord >=0)
  {
      for(int xCoord=0;xCoord<width;xCoord++)
          data[yCoord][xCoord]=0;
      yCoord--;
  }


}

public void performClear()
{
    int drop_ind = height-1;
  for(int yCoord=height-1;yCoord >= 0;yCoord--)
  {
      int nullCount=0;
    for(int xCoord=0;xCoord<width;xCoord++)
      if(data[yCoord][xCoord]==0)
          nullCount++;
    if(nullCount!=0)
    {

        if (nullCount == width) {
         while (drop_ind > yCoord ) {
             for (int xCoord = 0; xCoord < width; xCoord++)
                 data[drop_ind][xCoord]=0;
             drop_ind--;
         }
            break;
        }

            if(drop_ind>yCoord)
            {
                for(int xCoord=0;xCoord<width;xCoord++)
                    data[drop_ind][xCoord] = data[yCoord][xCoord];

            }
            drop_ind--;

    }else droppedLines++;


  }

  clearAbowe(drop_ind);
}

    public void drawGameField(Canvas canvas)
    {

        float canv_width = canvas.getWidth();
        float canv_height = canvas.getHeight();
        float freespacewidth = 0;
        float freespaceheight = 0;
        if(canv_width/canv_height < (float)fieldwidth/(float)fieldheight ) {
            cellsize = canv_width / (float) fieldwidth;
            freespacewidth = 0;
            freespaceheight = (canv_height - (float) fieldheight*cellsize)/2;
        }
        else {
            cellsize = canv_height / (float) fieldheight;
            freespaceheight = 0;
            freespacewidth = (canv_width - (float) fieldwidth*cellsize)/2;

        }


        canvas.drawRect(freespacewidth, freespaceheight,canv_width - freespacewidth,canv_height- freespaceheight,mBackGround);



        for(int colindex=0;colindex<=fieldheight;colindex++)
            canvas.drawLine(freespacewidth,colindex*cellsize+ freespaceheight,canv_width- freespacewidth,colindex*cellsize+ freespaceheight,mLinePaint);
        //for(int colindex=0;colindex<=fieldwidth;colindex++)
        for(int rowindex=0;rowindex<=fieldwidth;rowindex++)
            canvas.drawLine(rowindex*cellsize+ freespacewidth, freespaceheight,rowindex*cellsize+ freespacewidth,canv_height - freespaceheight,mLinePaint);



        Paint imgPaint = new Paint();

        //imgPaint.sem`
        Rect srcRect = new Rect(0,0, squareBitmaps[0].getWidth(),squareBitmaps[0].getHeight());


        for(int rowindex=0;rowindex<fieldwidth;rowindex++)
            for(int colindex=0;colindex<fieldheight;colindex++)
            {

                int cellData = data[colindex][ rowindex];
                if (cellData > 0) {
                    float left = freespacewidth + cellsize * rowindex;
                    float top = freespaceheight + cellsize * colindex;
                    //canvas.drawRect(left, top,left+cellsize,top+cellsize,mFigureColors[cellData-1]);
                 //   Bitmap bitmap = new Bitmap();
                    // 	drawBitmap(Bitmap bitmap, Rect src, RectF dst, Paint paint)

                    canvas.drawBitmap( squareBitmaps[cellData-1], srcRect , new RectF( left,  top,left+cellsize,top+cellsize), imgPaint);
                }

            }

if(showLevel)
{

    String levelText = "Level:" + String.valueOf(gameLevel) + " Lines:" +String.valueOf(droppedLines);

    setTextSizeForWidth(mTextColor,  canv_width -2*freespacewidth - 20,"00000000000000000000000000");
    canvas.drawText(levelText, freespacewidth+10,    freespaceheight  + cellsize + 10 , mTextColor);

}


        if(game_text.length > 0) {
       float theight =  setTextSizeForWidth(mTextColor,canv_width - 2* freespacewidth - 40,game_text[largest_row] );
       float base_height = canv_height/2 - game_text.length*(20+theight)/2;
       for(int rowInd = 0;rowInd<game_text.length;rowInd++)
            canvas.drawText(game_text[rowInd], freespacewidth +20,    base_height + rowInd * (20+theight) , mTextColor);
        }


    }




/*
    private  float setTextSizeForHeight(Paint paint, float desiredHeight,
                                       String text) {

        // Pick a reasonably large value for the test. Larger values produce
        // more accurate results, but may cause problems with hardware
        // acceleration. But there are workarounds for that, too; refer to
        // http://stackoverflow.com/questions/6253528/font-size-too-large-to-fit-in-cache
        final float testTextSize = 48f;

        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(testTextSize);
        // Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), textBounds);

        // Calculate the desired size as a proportion of our testTextSize.
        float desiredTextSize = testTextSize * desiredWidth / textBounds.width();

        // Set the paint for that size.
        paint.setTextSize(desiredTextSize);
        paint.getTextBounds(text, 0, text.length(), textBounds);
        return textBounds.height();
    }
    */

    private  float setTextSizeForWidth(Paint paint, float desiredWidth,
                                            String text) {

        // Pick a reasonably large value for the test. Larger values produce
        // more accurate results, but may cause problems with hardware
        // acceleration. But there are workarounds for that, too; refer to
        // http://stackoverflow.com/questions/6253528/font-size-too-large-to-fit-in-cache
        final float testTextSize = 48f;

        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(testTextSize);
       // Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), textBounds);

        // Calculate the desired size as a proportion of our testTextSize.
        float desiredTextSize = testTextSize * desiredWidth / textBounds.width();

        // Set the paint for that size.
        paint.setTextSize(desiredTextSize);
        paint.getTextBounds(text, 0, text.length(), textBounds);
        return textBounds.height();
    }


}
