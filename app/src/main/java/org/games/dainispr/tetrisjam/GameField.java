package org.games.dainispr.tetrisjam;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import static android.view.KeyEvent.ACTION_UP;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_POINTER_DOWN;
import static android.view.MotionEvent.ACTION_POINTER_UP;
import static java.lang.StrictMath.abs;


class GameField extends View {



private GameManager mGameManager;
private int pointerId = -1;
private int pointerIdForMove = -1;
private float moveX= 0;
private float moveY=0;
private boolean canDrop = true;
    public GameField(  Context context, AttributeSet attrs) {
        super(context, attrs);


    }

public void setGameManager(final GameManager gameManager)
{

    mGameManager = gameManager;


    setOnKeyListener(

   new View.OnKeyListener(){

       @Override
       public boolean onKey(View view, int i, KeyEvent keyEvent) {
     if (i == KeyEvent.KEYCODE_VOLUME_DOWN)
           {
              // Log.w("LightWriter", "I WORK BRO.");
               return true;
           }

           return false;
       }
   }



   );





    setOnTouchListener(

            new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                int pointerCount =    motionEvent.getPointerCount();
                    if(pointerCount >1 )
                        return false;
                 int actionPerformed = motionEvent.getActionMasked();


                if(actionPerformed == ACTION_DOWN )

                {
                    if (gameManager.isGameStarted() && !gameManager.isGamePaused()   ) {
                        /*
                        if (!gameManager.isOnFigure(motionEvent.getX(), motionEvent.getY()))
                            return false;
                        */
                        int pointerIndex = motionEvent.getActionIndex();
                        pointerId = motionEvent.getPointerId(pointerIndex);
                        moveX = motionEvent.getX();
                        moveY = motionEvent.getY();
                        canDrop = true;
                        return true;
                    }
                else {
                     if(!gameManager.isGamePaused())

                        gameManager.startNewGame();
                    else
                        gameManager.setGamePaused(false,0);
                    }
                }





                    if(actionPerformed == ACTION_MOVE)
                    {
                        int pointerIndex = motionEvent.getActionIndex();
                        float xMoved = motionEvent.getX() -  moveX;
                        if(pointerId >= 0  && pointerId == motionEvent.getPointerId(pointerIndex) )
                        {


                            float xFigMoved =   mGameManager.tryMove( xMoved);
                           if(xFigMoved != 0) {

                               pointerIdForMove = pointerId;
                               pointerId = -1;
                               moveX += xFigMoved;
                               canDrop = false;
                           }else
                           {

                              // pointerIdForMove = pointerId;
                             //  pointerId = -1;
                           float yMoved =  motionEvent.getY()- moveY;
                               if(yMoved>0 && yMoved > abs(xMoved) && mGameManager.tryDrop(yMoved))
                               {
                                   pointerId = -1;

                               }

                               else if(yMoved<0 && -yMoved > abs(xMoved) && mGameManager.setGamePaused(true,yMoved)  ){


                                   pointerId = -1;
                               }


                           }

                        }else if(pointerIdForMove >=0 && pointerIdForMove == motionEvent.getPointerId(pointerIndex) )
                        {

                            float xFigMoved =   mGameManager.tryMove( xMoved);

                            if(xFigMoved != 0 ) {
                                moveX += xFigMoved;
                                canDrop = false;
                            }

                            else if(canDrop)
                            {

                                float yMoved =  motionEvent.getY()- moveY;
                                if(yMoved>0 && yMoved > abs(xMoved) && mGameManager.tryDrop(yMoved))
                                {
                                    pointerIdForMove = -1;

                                }

                                else if(yMoved<0 && -yMoved > abs(xMoved) && mGameManager.setGamePaused(true,yMoved)  ){


                                    pointerIdForMove = -1;
                                }




                            }


                        }
                       return true;
                    }


                    if(actionPerformed == ACTION_UP )
                    {
                        int pointerIndex = motionEvent.getActionIndex();
                      if(pointerId >= 0  && pointerId == motionEvent.getPointerId(pointerIndex) ) {

                          mGameManager.tryRotate(motionEvent.getX()-moveX, motionEvent.getY()-moveY);
                          pointerId = -1;
                      }

                        if(pointerIdForMove >=0 && pointerIdForMove == motionEvent.getPointerId(pointerIndex) ) {

                            mGameManager.tryMove( motionEvent.getX()- moveX  );
                            pointerIdForMove = -1;
                        }

                    }




                    return true;

                }
            }


    );
}


@Override
public boolean onKeyUp(int keyCode,
                       KeyEvent event)
{

    if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
    {
        // Log.w("LightWriter", "I WORK BRO.");
        return true;
    }
return false;

}


    @Override
    public boolean onKeyDown(int keyCode,
                           KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            // Log.w("LightWriter", "I WORK BRO.");
            return true;
        }
        return false;

    }

    @Override
    public boolean onKeyLongPress(int keyCode,
                             KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            // Log.w("LightWriter", "I WORK BRO.");
            return true;
        }
        return false;

    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    mGameManager.getFieldData().drawGameField(canvas);


    }



}