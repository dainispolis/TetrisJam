package org.games.dainispr.tetrisjam;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaRouter;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.View;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;
import java.lang.IllegalStateException;

import static java.lang.StrictMath.abs;

public class GameManager   {



    private class FigureShapes {
        public FigureShapes(int[][][] xCoords, int[][][] yCoords) {
            this.xCoords = xCoords;
            this.yCoords = yCoords;
        }

        public int[][][] xCoords; //rotation,figure,square
        public int[][][] yCoords;

        // public int [][][][] x

    }

    private int currentFigure = 0;
    private int figPosX = 0;
    private int figPosY = 0;
    private int figRot = 0;
   // private int fallingSpeed = 500;
    private int droppedLines;
    private MediaPlayer gameEndPlayer = null;
    private MediaPlayer newLevelPlayer = null;
    private MediaPlayer lineDropPlayer = null;
    private MediaPlayer figureRotPlayer = null;
    private MediaPlayer figureRightPlayer = null;
    private MediaPlayer figureLeftPlayer = null;
    private SharedPreferences gamePreferences;
    private boolean gameStarted = false;
    private boolean gamePaused = false;
    private int[] levelTable = {10,25,50,75,100};
    private int[] levelSpeeds = {350,300,250,210,180};
    private int gameLevel=0;
    private float soundLevel=1;


    private final FigureShapes mFigures = new FigureShapes(
            new int[][][]{
                    {
                            {0, 0, 0, 0},
                            {0, 1, 1, 1},
                            {0, 1, 0, 0},
                            {0, 0, 1, 1},
                            {1, 1, 0, 0},
                            {0, 0, 1, 0},
                            {0, 1, 0, 1}
                    },

                    {
                            {-1, 0, 1, 2},
                            {1, 1, 0, -1},
                            {0, 1, 2, 2},
                            {2, 1, 1, 0},
                            {0, 1, 1, 2},
                            {1, 0, 0, -1},
                            {0, 1, 0, 1}
                    },

                    {
                            {0, 0, 0, 0},
                            {1, 0, 0, 0},
                            {1, 1, 1, 0},
                            {0, 0, 1, 1},
                            {1, 1, 0, 0},
                            {0, 0, -1, 0},
                            {0, 1, 0, 1}
                    },

                    {
                            {-1, 0, 1, 2},
                            {0, 0, 1, 2},
                            {0, 0, 1, 2},
                            {2, 1, 1, 0},
                            {0, 1, 1, 2},
                            {1, 0, 0, -1},
                            {0, 1, 0, 1}
                    }

            }, new int[][][]{
            {
                    {0, 1, 2, 3},
                    {0, 0, 1, 2},
                    {0, 0, 1, 2},
                    {0, 1, 1, 2},
                    {0, 1, 1, 2},
                    {0, 1, 1, 2},
                    {0, 0, 1, 1}
            },

            {
                    {1, 1, 1, 1},
                    {0, 1, 1, 1},
                    {0, 0, 0, 1},
                    {0, 0, 1, 1},
                    {0, 0, 1, 1},
                    {1, 1, 2, 1},
                    {0, 0, 1, 1}
            },

            {
                    {0, 1, 2, 3},
                    {1, 1, 0, -1},
                    {0, 1, 2, 2},
                    {0, 1, 1, 2},
                    {0, 1, 1, 2},
                    {0, 1, 1, 2},
                    {0, 0, 1, 1}
            },

            {
                    {1, 1, 1, 1},
                    {1, 0, 0, 0},
                    {0, 1, 1, 1},
                    {0, 0, 1, 1},
                    {0, 0, 1, 1},
                    {1, 1, 0, 1},
                    {0, 0, 1, 1}
            }

    }

    );

    private Random rand = new Random();


    private GameFieldData mGameData;
    private Timer mTimer = null;
    private FullscreenActivity mActivity;


    public GameManager(int width, int height, FullscreenActivity activity) {


        mActivity = activity;
        gamePreferences = mActivity.getPreferences(Context.MODE_PRIVATE);

        mGameData = new GameFieldData(width, height, activity);
        String[] fieldText = new String[]{
                "Hi!,Welcome!",
                "Left:Drag left",
                "Right:Drag right",
                "Rotate/Resume:touch",
                "Drop:Drag down",
                "Pause:Drag up"

        };
        mGameData.setFieldText(fieldText);
        figPosX = width / 2;
        figPosY = 0;


        gameEndPlayer = MediaPlayer.create(mActivity.getApplicationContext(), R.raw.uz_to_mums);
        newLevelPlayer = MediaPlayer.create(mActivity.getApplicationContext(), R.raw.limenis);
        lineDropPlayer = MediaPlayer.create(mActivity.getApplicationContext(), R.raw.lines);
        figureRotPlayer = MediaPlayer.create(mActivity.getApplicationContext(), R.raw.pagriezens);
        figureLeftPlayer = MediaPlayer.create(mActivity.getApplicationContext(), R.raw.pa_kreisi);
        figureRightPlayer = MediaPlayer.create(mActivity.getApplicationContext(), R.raw.pa_labi);

    }

 public void volumeDown()
 {
     if(soundLevel <= 0)
         return;
  soundLevel -= 0.2;
  if(soundLevel<0)
      soundLevel =0;
     setSoundLevel();

 }

    public void volumeUp()
    {
        if(soundLevel >= 1)
            return;
        soundLevel += 0.2;
        if(soundLevel>1)
            soundLevel =1;
        setSoundLevel();

    }



    public GameFieldData getFieldData() {
        return mGameData;
    }




    public void startNewGame() {
        //  String [] fieldText = new String [] {};
        mGameData.setFieldText(new String[]{});
        mGameData.clearData();

        currentFigure = rand.nextInt(7);

        figPosX = mGameData.getWidth() / 2;
        figPosY = 0;
        figRot = 0;
        placeFigure();
        refreshField();
        gameLevel=0;
        mGameData.setGameLevel(gameLevel);
        startTimer(500);
        mGameData.setShowLevel(true);
        gameStarted = true;

    }

    public void tryRotate(float xPos, float yPos) {

        if (mGameData.cellRelative(xPos, yPos)>0.25)
            return;


        figureRotPlayer.start();
        rotate();
    }

    public float tryMove(float xDiff) {
        int xDiffInt = mGameData.diffInt(xDiff);
        if (xDiffInt == 0)
            return 0;

        clearFigure();

        if (canMove(figPosX + xDiffInt))
            figPosX += xDiffInt;
        else xDiffInt = 0;


        placeFigure();
        if (xDiffInt != 0) {


            if (xDiffInt > 0)
                figureRightPlayer.start();
            else
                figureLeftPlayer.start();


            refreshField();
        }
        return mGameData.diffFloat(xDiffInt);

    }

    public boolean tryDrop(float yDiff) {
        int yDiffInt = mGameData.diffInt(yDiff);
        if (yDiffInt == 0)
            return false;

        clearFigure();
        yDiffInt = 1;
        while (canPlace(figRot, currentFigure, figPosX, figPosY + yDiffInt))
            yDiffInt++;
        figPosY += yDiffInt - 1;
        // else xDiffInt = 0;
        placeFigure();
        if (yDiffInt > 1)
            refreshField();

        return yDiffInt > 1;

    }

    public boolean isOnFigure(float xPos, float yPos) {
        int curX = figPosX;
        int curY = figPosY;

        for (int figRect = 0; figRect < 4; figRect++) {
            int rectX = curX + mFigures.xCoords[figRot][currentFigure][figRect];
            int rectY = curY + mFigures.yCoords[figRot][currentFigure][figRect];
            if (mGameData.isOnField(xPos, yPos, rectX, rectY))
                return true;
        }

        return false;
    }

    public boolean isGameStarted() {

        return gameStarted;
    }

    public boolean setGamePaused(boolean paused, float distance) {
        if (paused && mGameData.diffInt(distance) == 0)
            return gamePaused;
        gamePaused = paused;
        if (gamePaused) {
            mGameData.setFieldText(new String[]{"Paused, touch to continue!"});
            refreshField();
        } else
            mGameData.setFieldText(new String[]{});
        return paused;
    }

    public boolean isGamePaused() {

        return gamePaused;
    }


    private void setSoundLevel()
    {

        gameEndPlayer.setVolume(soundLevel,soundLevel );
        newLevelPlayer.setVolume(soundLevel,soundLevel );
        lineDropPlayer.setVolume(soundLevel,soundLevel );
        figureRotPlayer.setVolume(soundLevel,soundLevel );
        figureLeftPlayer.setVolume(soundLevel,soundLevel );
        figureRightPlayer.setVolume(soundLevel,soundLevel );

    }

    private void startTimer(int period) {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            public void run() {

                if (!gamePaused && !moveDown() && !createNext()) {
                    //   mGameData.getDroppedLines();
                    mTimer.cancel();
                    gameStarted = false;
                    mGameData.setShowLevel(false);
                    gameEndPlayer.start();

                    int prev_record = gamePreferences.getInt("dropped_lines", 0);

                    if (mGameData.getDroppedLines() > prev_record) {
                        mGameData.setFieldText(new String[]{"Wonderfull!, You have", "record, you dropped ", mGameData.getDroppedLines() + " lines!"});
                        SharedPreferences.Editor prefEditor = gamePreferences.edit();
                        prefEditor.putInt("dropped_lines", mGameData.getDroppedLines());

                        prefEditor.apply();
                    } else
                        mGameData.setFieldText(new String[]{"Great!, You dropped", mGameData.getDroppedLines() + " lines!"});
                    refreshField();
                } else {
                    int new_droppedLines = mGameData.getDroppedLines();
                    if (new_droppedLines > droppedLines) {

                        lineDropPlayer.start();

                    }
                    droppedLines = new_droppedLines;
                    // private int[] levelTable = {10,25,50,75,100};
                    //    private int[] levelSpeeds = {350,300,250,210,180};

                    if(  gameLevel < 4 &&   droppedLines >= levelTable[gameLevel]    )
                    {

                        mTimer.cancel();

                        newLevelPlayer.start();
                    startTimer(levelSpeeds[gameLevel]);
                    gameLevel++;
                        mGameData.setGameLevel(gameLevel);
                    }




                }
            }
        }, 0, period);
    }


    protected boolean canPlace(int rot, int figure, int xCoord, int yCoord) {

        for (int figRect = 0; figRect < 4; figRect++) {
            int rectX = xCoord + mFigures.xCoords[rot][figure][figRect];
            int rectY = yCoord + mFigures.yCoords[rot][figure][figRect];
            if (
                    rectX >= mGameData.getWidth() ||
                            rectY >= mGameData.getHeight() ||
                            rectX < 0 ||
                            rectY < 0 ||
                            mGameData.getFieldData(rectX, rectY) > 0) {
                return false;
            }

        }


        return true;

    }


    protected boolean canMove( int xCoordEnd) {
//mTimer.cancel();
    int xDelta = Integer.signum(xCoordEnd-figPosX);
   for(int xMoved=figPosX+xDelta;xMoved!=xCoordEnd+xDelta;xMoved += xDelta ) {
       for (int figRect = 0; figRect < 4; figRect++) {
           int rectX =xMoved + mFigures.xCoords[figRot][currentFigure][figRect];
           int rectY = figPosY + mFigures.yCoords[figRot][currentFigure][figRect];
           if (
                   rectX >= mGameData.getWidth() ||
                           rectY >= mGameData.getHeight() ||
                           rectX < 0 ||
                           rectY < 0 ||
                           mGameData.getFieldData(rectX, rectY) > 0) {
               return false;
           }

       }
   }

        return true;

    }



    protected boolean createNext() {
        figPosX = mGameData.getWidth() / 2;
        figPosY = 0;
        figRot = 0;
        currentFigure = rand.nextInt(7);

        if (!canPlace(figRot, currentFigure, figPosX, figPosY))
            return false;

        placeFigure();
        refreshField();
        return true;
    }


    protected void refreshField() {
        mActivity.runOnUiThread(

                new Runnable() {

                    @Override
                    public void run() {

                        mActivity.refreshGamefield();

                    }
                }

        );


    }


    protected boolean moveDown() {

        clearFigure();
        int diffY = 0;
        boolean stopshedule = false;
/*
if(figPosY > 16)
    cancel();
*/
        if (canPlace(figRot, currentFigure, figPosX, figPosY + 1))
            diffY = 1;
        figPosY += diffY;
        placeFigure();

        if (diffY == 0) {
            // cancel();
            mGameData.performClear();
            // mTimer.scheduleAtFixedRate(this,0,1000);
        }

        refreshField();

//      if(stopshedule  )


        return diffY != 0;
    }

    protected void rotate() {

        clearFigure();

        int newRot = figRot;
        newRot++;
        if (newRot > 3)
            newRot = 0;


        if (canPlace(newRot, currentFigure, figPosX, figPosY)) {
            figRot = newRot;
            placeFigure();

            refreshField();

        }
        else
            placeFigure();

    }

    protected void placeFigure() {


        for (int figRect = 0; figRect < 4; figRect++) {
            mGameData.setFieldData(figPosX + mFigures.xCoords[figRot][currentFigure][figRect],
                    figPosY + mFigures.yCoords[figRot][currentFigure][figRect],
                    currentFigure + 1
            );
        }
    }


    protected void clearFigure() {
        for (int figRect = 0; figRect < 4; figRect++) {

            mGameData.setFieldData(figPosX + mFigures.xCoords[figRot][currentFigure][figRect],
                    figPosY + mFigures.yCoords[figRot][currentFigure][figRect],
                    0
            );


        }

    }



}
